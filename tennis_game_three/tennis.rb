require_relative 'player'

class TennisGame3
  POINTS = {0 => "Love", 1 => "Fifteen", 2 => "Thirty", 3 => "Forty"}

  def initialize(player_1_name, player_2_name)
    @player_one = Player.new(player_1_name)
    @player_two = Player.new(player_2_name)
  end
      
  def won_point(name)
    @player_one.check_point(name)
    @player_two.check_point(name)
  end
  
  def score
    if (@player_one.score < 4 and @player_two.score < 4) and (@player_one.score + @player_two.score < 6)

      score = POINTS[@player_one.score]
      @player_one.score == @player_two.score ? score + "-All" : score + "-" + POINTS[@player_two.score]
    else
      if @player_one.score == @player_two.score
        "Deuce"
      else
        score = @player_one.score > @player_two.score ? @player_one.name : @player_two.name
        (@player_one.score-@player_two.score)*(@player_one.score-@player_two.score) == 1 ? "Advantage " + score : "Win for " + score
      end
    end
  end
end
