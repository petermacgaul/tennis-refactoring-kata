require_relative 'score'

class Player
  attr_reader :name

  def initialize(name)
    @score = Score.new
    @name = name
  end

  def check_point(name)
    if name == @name
      @score.won_point
    end
  end

  def score
    @score.score
  end

end
