class Score
  attr_reader :score

  def initialize
    @score = 0
  end

  def won_point
    @score += 1
  end
end

class ScoreZero
  attr_reader :score

  def initialize
    @score = 0
  end

  def won_point
    ScoreFifteen.new
  end
end

class ScoreFifteen
  attr_reader :score

  def initialize
    @score = 1
  end

  def won_point
    ScoreThirty.new
  end
end

class ScoreThirty
  attr_reader :score

  def initialize
    @score = 2
  end

  def won_point
    ScoreThirty.new
  end
end

class ScoreForty
  attr_reader :score

  def initialize
    @score = 3
  end
end